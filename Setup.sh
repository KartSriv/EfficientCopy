#!/bin/bash

# ROOTorNOT program of ___________ Version 2.0.1
if [ "$EUID" -ne 0 ]
  then echo "You ain't Superuser(Root) my friend. Just type \"sudo\" before launching the script. "
  echo "Make this script work, mate!"
  exit
else
  cd /
  mkdir /EfficientCopy/
  mkdir /EfficientCopy/tmp
  mkdir /EfficientCopy/clipboard
  touch /EfficientCopy/clipboard/Clipboard.txt
  cd /EfficientCopy/tmp
  wget -L https://raw.githubusercontent.com/KartSriv/EfficientCopy/master/SettingUpForBashBoot.sh
  sudo sh ./SettingUpForBashBoot.sh
fi
