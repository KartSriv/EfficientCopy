#!/usr/bin/env python

# This is EfficientCopy 1.1
# A utility in Synergy by @karthiksrivijay

# Modules
import os # For system commands
import sys # For beginning statement
import subprocess # Storing Outputs

if len(sys.argv) >= 1: # Only Copy
    Copy_FileName = sys.argv[1]
    Full_FileName = subprocess.check_output("realpath " + Copy_FileName, shell=True) # Full Path
    FileName_Re = subprocess.check_output("basename " + Full_FileName, shell=True) # Only FileName
    # Check If File exists
    if os.path.exists(Full_FileName) == False: # No File
        print "OOPS! The file doesn't exist, mate!"
        print "ERROR 003"
        exit
    os.system("echo '" + Full_FileName + "' >> /EfficientCopy/clipboard/Clipboard.txt")
    print "File \"" + FileName_Re + "\" is copied to the clipboard!"
elif len(sys.argv) >= 2: # Copy and Paste
    Copy_FileName = sys.argv[1]
    Full_FileName = subprocess.check_output("realpath " + Copy_FileName, shell=True) # Full Path
    FileName_Re = subprocess.check_output("basename " + Full_FileName, shell=True) # Only FileName
    # Check If File exists
    if os.path.exists(Full_FileName) == False: # No File
        print "OOPS! The copy file doesn't exist, mate!"
        print "ERROR 004"
        exit
    Paste_FileName = sys.argv[2]
    Full_FileName_Paste = subprocess.check_output("realpath " + Paste_FileName, shell=True) # Full Path
    FileName_De = subprocess.check_output("basename " + Full_FileName_Paste, shell=True) # Only FileName
    # Check If File exists
    if os.path.exists(Full_FileName_Paste) == False: # No File
        print "OOPS! The paste file doesn't exist, mate!"
        print "ERROR 005"
        exit
    os.system("cp " + Full_FileName + " " + Full_FileName_Paste)
    print "File " + FileName_Re + " was copied to " + FileName_De + "."
    exit
elif len(sys.argv) == 0: # No Options
    print "OOPS! You got no options over here, mate!"
    print "ERROR 001"
elif len(sys.argv) > 0: # Program ERROR!
    print "OOPS! Program Correpted, mate!"
    print "ERROR 002"
