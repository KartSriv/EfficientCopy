#!/bin/bash

echo "Welcome to EazyMove!"
echo "Packages to be installed:-"
echo "1) Atom"
echo "2) Ubuntu Make"
echo "3) Visual Studio Code"
echo "4) PIP for Python 2.x and Python 3.x"
echo "5) Git for Linux"
echo "6) espeak"
echo "7) Google Chrome"
echo "8) JetBrains PyCharm"
echo "9) Deepin Desktop"


# Atom

sudo add-apt-repository ppa:webupd8team/atom # Repository for Atom

sudo apt-get update # Updating

sudo apt-get install atom # Installing Atom

# Ubuntu Make

sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make # Repository for Ubuntu Make

sudo apt-get update # Updating

sudo apt-get install ubuntu-make # Installing ubuntu-make

# Visual Studio Code

umake ide visual-studio-code # Installing Visual Studio Code

# PIP for Python 2.x and Python 3.x

sudo apt-get install python-pip

# Git for linux

sudo apt install git

# espeak 

sudo apt install espeak

# Google Chrome

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 

sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

sudo apt-get update 

sudo apt-get install google-chrome-stable

#PyCharm

umake ide pycharm

#Sublime Text

sudo add-apt-repository ppa:webupd8team/sublime-text-3
sudo apt-get update
sudo apt-get install sublime-text-installer

#Deepin Desktop

#sudo add-apt-repository ppa:leaeasy/dde
#sudo apt-get update
#sudo apt-get install dde

#Unity Tweak Tool

sudo apt-get install unity-tweak-tool

#Ubuntu Tweak

wget -q -O - http://archive.getdeb.net/getdeb-archive.key | sudo apt-key add -
sudo sh -c 'echo "deb http://archive.getdeb.net/ubuntu xenial-getdeb apps" >> /etc/apt/sources.list.d/getdeb.list'
sudo apt-get update
sudo apt-get install ubuntu-tweak

#Flat Unity Theme

sudo add-apt-repository ppa:noobslab/themes
sudo apt-get update
sudo apt-get install flatabulous-theme

