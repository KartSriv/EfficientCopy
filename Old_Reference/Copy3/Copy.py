#!/usr/bin/env python

# This is FastCopy 1.3
# A utility in Synergy by @karthiksrivijay

# Modules
import os # For system commands
import sys # For beginning statement
import subprocess # Storing Outputs


# Taking the user input from statement
# For more information check: https://stackoverflow.com/questions/44024686/how-to-take-input-in-beginning-of-the-program-in-python

FileName = sys.argv[1] # Using sys Modules

# Finding current working directory
# For more information check: https://stackoverflow.com/questions/8659275/how-to-store-the-result-of-an-executed-shell-command-in-a-variable-in-python
Output = subprocess.check_output("readlink -f " + FileName, shell=True)

# Creating variable with location
Location=Output

# Writing to Clipboard
Copy = open("/Clipboard/location.txt", 'w')
Copy.truncate()
Copy.write(Location)
Copy.close()

# Telling the user that it's successful.
# For more information check: https://learnpythonthehardway.org/book/ex5.html
print "Status: "+FileName+" has been copied to the clipboard."
