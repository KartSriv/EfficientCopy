#!/usr/bin/env python

# This is FastMount 1.1
# A utility in Synergy by @karthiksrivijay

# Modules
import os # For system commands
import sys # For beginning statement
import subprocess # Storing Outputs


# Taking the user input from statement
# For more information check: https://stackoverflow.com/questions/44024686/how-to-take-input-in-beginning-of-the-program-in-python

FileName = sys.argv[1] # Using sys Modules

# Finding current working directory
# For more information check: https://stackoverflow.com/questions/8659275/how-to-store-the-result-of-an-executed-shell-command-in-a-variable-in-python
Output = subprocess.check_output("pwd", shell=True)

# Creating variable with location
Location=Output+"/"+FileName
print Location

# Writing to Clipboard
FH = open("/Clipboard/location.txt")
full_text = FH.read()
all_lines = full_text.split('\n')
for line in all_lines :
      Location=line
FH.close()

# Telling the user that it's successful.
# For more information check: https://learnpythonthehardway.org/book/ex5.html
print "Status: "+Location+" has been copied to the clipboard."
