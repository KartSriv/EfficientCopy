#!/usr/bin/env python

# This is EfficientCopy 1.1
# A utility created by @karthiksrivijay

import os # For system commands
import sys # For beginning statement
import subprocess # Storing Outputs

# Taking the user input from statement
# For more information check: https://stackoverflow.com/questions/44024686/how-to-take-input-in-beginning-of-the-program-in-python
Location = sys.argv[1] # Using sys Modules


# Check If FileName Given
if sys.argv[1] == "":
    print "OOPS! You gotta specify what to copy, mate!"
    print "ERROR 001"
    exit

Location = subprocess.check_output("realpath " + Location, shell=True) # Full Path

# Check If File exists
if os.path.exists(Location) == False:
    print "OOPS! The file doesn't exist, mate!"
    print "ERROR 002"
    exit

FileName_Re = subprocess.check_output("basename " + Location, shell=True) # File Name

# Check If Destinaton is given
if sys.argv[2] == "":
    # Adding to Clipboard
    os.system("echo '"+ Location + "' >> /EfficientCopy/clipboard/Clipboard.txt")

    # Telling the user that it's successful.
    # For more information check: https://learnpythonthehardway.org/book/ex5.html
    print "Status: "+Location+" has been copied to the clipboard."


Destinaton = sys.argv[2]
FileName_De = subprocess.check_output("basename " + Destinaton, shell=True) # Folder Name
Destinaton = subprocess.check_output("realpath " + Destinaton, shell=True) # Full Path
# Check If File exists
if os.path.exists(Destinaton) == False:
    print "OOPS! The Destinaton Location doesn't exist, mate!"
    print "ERROR 003"
    exit
elif os.path.exists(Destinaton) == True:
    os.system("cp " + Location + " " + Destinaton)
    print "File " + FileName_Re + " was copied to " + FileName_De + "."
    exit
