#!/usr/bin/env python

# This is EfficientCopy 1.1
# A utility created by @karthiksrivijay

# Modules
import os # For system commands
import sys # For beginning statement
import subprocess # Storing Outputs
#from pathlib import Path # File Check

# Taking the user input from statement
# For more information check: https://stackoverflow.com/questions/44024686/how-to-take-input-in-beginning-of-the-program-in-python
FileName = sys.argv[1] # Using sys Modules

# Check If FileName Given
if sys.argv[1] == "":
    print "OOPS! You gotta specify what to copy, mate!"
    exit

# Check If Paste Location Given
if sys.argv[2] != "":
    Dest = sys.argv[2]
    os.system


# Finding current working directory
# For more information check: https://stackoverflow.com/questions/8659275/how-to-store-the-result-of-an-executed-shell-command-in-a-variable-in-python
Location = subprocess.check_output("realpath " + FileName, shell=True)

print Location
